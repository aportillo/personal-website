package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

const GITLAB_PROJECT_ENDPOINT = "https://gitlab.com/api/v4/projects"
const GITLAB_ACCESS_TOKEN_ENV_VAR_KEY = "GITLAB_ACCESS_TOKEN"
const IMG_DIRECTORY = "./ui/static/img/"
const STATIC_IMG_DIRECTORY = "static/img/"

type Projects []Project

type Project struct {
	Description string `json:"description"`
	Name        string `json:"name"`
	WebURL      string `json:"web_url"`
}

// serverError returns 500 internal server error.
func (app *application) serverError(w http.ResponseWriter, r *http.Request, err error) {
	method := r.Method
	uri := r.URL.RequestURI()
	log.Println(err.Error(), "method", method, "uri", uri)
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

// clientError is a general function for returning a client error.
func (app *application) clientError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}

// notFound returns a specific 404 not found error.
func (app *application) notFound(w http.ResponseWriter) {
	app.clientError(w, http.StatusNotFound)
}

// getProjects returns a list of the user's public Gitlab projects.
func getProjects(apiUrl string) (Projects, error) {
	resp, err := http.Get(apiUrl)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var projects Projects
	err = json.Unmarshal(body, &projects)
	if err != nil {
		return nil, err
	}

	return projects, nil
}

// createURL returns the Gitlab api endpoint including query parameters.
func createURL(baseURL string) (*string, error) {
	apiKey := os.Getenv(GITLAB_ACCESS_TOKEN_ENV_VAR_KEY)
	if len(apiKey) == 0 {
		log.Println("gitlab api key environment variable not set")
	}

	params := map[string]string{
		"private_token": apiKey,
		"owned":         "true",
		"visibility":    "public",
	}

	u, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}

	q := u.Query()
	for k, v := range params {
		q.Set(k, v)
	}
	u.RawQuery = q.Encode()
	urlWithParams := u.String()

	return &urlWithParams, nil
}

// cleanNames converts a kebab case string to spaces instead of dashes.
func cleanNames(name string) string {
	return strings.ReplaceAll(name, "-", " ")
}

// getPhotoPaths returns a slice of all the photos in the img directory.
func getPhotoPaths() ([]string, error) {
	result := []string{}

	entries, err := os.ReadDir(IMG_DIRECTORY)
	if err != nil {
		return nil, err
	}

	for _, file := range entries {
		if strings.HasSuffix(file.Name(), "jpg") {
			result = append(result, fmt.Sprintf("%s%s", STATIC_IMG_DIRECTORY, file.Name()))
		}
	}

	return result, nil
}

// newTemplateCache creates a cache of templates to be generated when server is started.
func newTemplateCache() (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	pages, err := filepath.Glob("./ui/html/pages/*.html")
	if err != nil {
		return nil, err
	}

	for _, page := range pages {
		name := filepath.Base(page)
		files := []string{
			"./ui/html/base.html",
			"./ui/html/partials/header.html",
			page,
		}

		funcMap := template.FuncMap{
			"cleanName": cleanNames,
		}

		tmpl := template.New("")
		tmpl.Funcs(funcMap)

		ts, err := tmpl.ParseFiles(files...)
		if err != nil {
			return nil, err
		}

		cache[name] = ts
	}
	return cache, nil
}

// render loads the appropriate template and executes it.
func (app *application) render(w http.ResponseWriter, r *http.Request, status int, page string, data templateData) {
	tmpl, ok := app.templateCache[page]
	if !ok {
		err := fmt.Errorf("the template %s does not exist", page)
		app.serverError(w, r, err)
		return
	}

	w.WriteHeader(status)

	err := tmpl.ExecuteTemplate(w, "base", data)
	if err != nil {
		app.serverError(w, r, err)
	}
}
