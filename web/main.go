package main

import (
	"html/template"
	"log"
	"net/http"
	"os"
)

type application struct {
	templateCache map[string]*template.Template
}

func main() {
	templateCache, err := newTemplateCache()
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	app := &application{
		templateCache: templateCache,
	}

	log.Println("starting server on :8080")
	err = http.ListenAndServe(":8080", app.routes())
	log.Fatal(err)
	os.Exit(1)
}
