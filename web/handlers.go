package main

import (
	"net/http"
)

type templateData struct {
	Projects Projects
	Photos   []string
	Page     string
}

// Handler for / route.
func (app *application) home(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		app.notFound(w)
		return
	}

	data := templateData{
		Page: "home",
	}

	app.render(w, r, http.StatusOK, "main.html", data)
}

// Handler for /projects route.
func (app *application) projects(w http.ResponseWriter, r *http.Request) {
	apiUrl, err := createURL(GITLAB_PROJECT_ENDPOINT)
	if err != nil {
		app.serverError(w, r, err)
	}

	projects, err := getProjects(*apiUrl)
	if err != nil {
		app.serverError(w, r, err)
	}

	data := templateData{
		Projects: projects,
		Page:     "projects",
	}

	app.render(w, r, http.StatusOK, "projects.html", data)
}

// Handler for /photography route.
func (app *application) photography(w http.ResponseWriter, r *http.Request) {
	photoPaths, err := getPhotoPaths()
	if err != nil {
		app.serverError(w, r, err)
		return
	}

	data := templateData{
		Photos: photoPaths,
		Page:   "photography",
	}

	app.render(w, r, http.StatusOK, "photography.html", data)
}
