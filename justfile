set dotenv-load

run:
	watchexec -e go,css,html -c -r -- 'tailwindcss -i ./ui/static/css/base.css -o ./ui/static/css/main.css; go run ./web'

build:
	# os/arch variables required for my specific web host
	GOOS=freebsd GOARCH=amd64 go build -C web -o $WEB_SERVER_DIRECTORY'/server'

upload-build: build
	scp server $WEB_SERVER_USER'@'$WEB_SERVER_ADDRESS':/home/protected'

upload-assets:
	scp -r ui $WEB_SERVER_USER'@'$WEB_SERVER_ADDRESS':/home/public/'

deploy: upload-build upload-assets

css:
	tailwindcss -i ./ui/static/css/base.css -o ./ui/static/css/main.css

ssh:
	ssh $WEB_SERVER_USER'@'$WEB_SERVER_ADDRESS
